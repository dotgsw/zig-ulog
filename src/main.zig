const std = @import("std");
const Parser = @import("lib.zig").Parser;
const Allocator = std.mem.Allocator;

pub fn readSource(gpa: Allocator, input_filename: []const u8) ![:0]u8 {
    var file = try std.fs.cwd().openFile(input_filename, .{});
    defer file.close();
    const stat = try file.stat();

    const source = try gpa.allocSentinel(u8, @intCast(stat.size), 0);
    const size = try file.readAll(source);
    if (stat.size != size) {
        std.log.err("Failed to read entire source file", .{});
        std.os.exit(1);
    }

    return source;
}

pub fn main() !void {
    var allocator = std.heap.GeneralPurposeAllocator(.{}){};
    defer std.debug.assert(allocator.deinit() == .ok);
    const gpa = allocator.allocator();

    var parser = Parser.init(gpa);
    defer parser.deinit();

    const file = try readSource(gpa, "src/16_11_14.ulg");
    try parser.parse(file);
}
