// toplevel container for a parsed log file
const std = @import("std");
const Allocator = std.mem.Allocator;
const StringView = @import("StringView.zig");
const Parser = @import("Parser.zig");

const Container = @This();
const builtin_types = Parser.builtin_types;

gpa: Allocator,
timestamp: u64,
str_bytes: []const u8,
subscriptions: []const Subscription,
fields: []const Field,
types: std.ArrayHashMapUnmanaged(StringView, FieldSet, StringView.ArrayContext, true),
logs: []const Log,
dropouts: []const Dropout,

pub const Subscription = extern struct {
    multi_id: u8,
    message_type: Type,
    message_size: usize,
    message_count: usize,
    data_ptr: [*c]align(@alignOf(u64)) const u8,
    data_len: usize,
};

pub const Field = extern struct {
    name_ptr: [*c]const u8,
    name_len: usize,
    type: Type,
    /// 1 for scalar fields, greater than 1 for arrays
    count: usize,

    pub const Iterator = struct {
        // if true, recurses into nested types to only yield primitive fields
        recursive: bool,
    };
};

pub const FieldSet = extern struct {
    start: u32,
    end: u32,
};

pub const Dropout = extern struct {
    last_received: u64,
    duration: u64,
};

pub const Log = extern struct {
    timestamp: u64,
    level: Level,
    message_ptr: [*c]const u8,
    message_len: usize,

    pub const Level = enum(u8) {
        emerg,
        alert,
        crit,
        err,
        warning,
        notice,
        info,
        debug,
        _,
    };
};

pub const Type = enum(u32) {
    int8,
    uint8,
    int16,
    uint16,
    int32,
    uint32,
    int64,
    uint64,
    float,
    double,
    bool,
    char,
    _,
};

pub fn deinit(self: *Container) callconv(.C) void {
    self.gpa.free(self.str_bytes);
    self.gpa.free(self.subscriptions);
    self.gpa.free(self.fields);
    self.types.deinit(self.gpa);
    self.gpa.free(self.logs);
    self.gpa.free(self.dropouts);
}

pub inline fn getHandle(self: *Container, type_name: []const u8) ?Type {
    if (builtin_types.get(type_name)) |builtin_type| {
        return builtin_type;
    }

    const adapter: StringView.ArraySliceAdapter = .{ .str_bytes = self.str_bytes };
    const handle = self.types.getIndexAdapted(type_name, adapter);
    if (handle) |user_type| {
        return @enumFromInt(@as(u32, @intCast(user_type)) + std.meta.fields(Type).len);
    }

    return null;
}

pub fn getSize(self: *Container, ty: Type) callconv(.C) usize {
    return switch (ty) {
        .int8, .uint8 => 1,
        .int16, .uint16 => 2,
        .int32, .uint32 => 4,
        .int64, .uint64 => 8,
        .float => 4,
        .double => 8,
        .bool, .char => 1,
        else => {
            const index = @intFromEnum(ty) - std.meta.fields(Type).len;
            const set = self.types.values()[index];

            const fields = self.fields[set.start..set.end];
            var total: usize = 0;
            for (fields) |field| {
                total += self.getSize(field.type) * field.count;
            }

            return total;
        },
    };
}

pub fn findSubscription(self: *Container, ty: Type) !u32 {
    for (self.subscriptions, 0..) |subscription, i| {
        if (subscription.message_type == ty) return @intCast(i);
    }

    return error.UnknownSubscription;
}

pub inline fn fieldset(self: *Container, ty: Type) ?FieldSet {
    const index: u32 = @intFromEnum(ty);
    const builtin_count = std.meta.fields(Type).len;
    if (index < builtin_count) return null;
    if (index - builtin_count >= self.types.entries.len) return null;
    return self.types.entries.items(.value)[index - builtin_count];
}
