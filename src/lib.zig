const std = @import("std");
pub const Container = @import("Container.zig");
pub const Parser = @import("Parser.zig");

pub const ulg_parser = Parser;
pub const ulg_container = Container;
pub const ulg_subscription = Container.Subscription;
pub const ulg_field = Container.Field;
pub const ulg_type = Container.Type;
pub const ulg_dropout = Container.Dropout;
pub const ulg_log = Container.Log;
pub const ulg_log_level = Container.Log.Level;
pub const ulg_fieldset = Container.FieldSet;

pub const ulg_result = enum(i32) {
    ULG_RESULT_OK = 0,
    ULG_RESULT_OUT_OF_MEMORY = -1,
    ULG_RESULT_INVALID_MAGIC = -2,
    ULG_RESULT_INVALID_VERSION = -3,
    ULG_RESULT_UNKNOWN_TYPE = -4,
    ULG_RESULT_INVALID_FIELD_FORMAT = -5,
    ULG_RESULT_UNKNOWN_SUBSCRIPTION = -6,
    ULG_RESULT_PRIMITIVE_TYPE_FIELDS = -7,
};

comptime {
    @export(Container.deinit, .{ .name = "ulg_container_destroy", .linkage = .strong });
    @export(Container.getSize, .{ .name = "ulg_container_get_type_size", .linkage = .strong });
}

export fn ulg_parser_create(out_parser: **ulg_parser) ulg_result {
    const parser = std.heap.c_allocator.create(ulg_parser) catch |err| switch (err) {
        error.OutOfMemory => return .ULG_RESULT_OUT_OF_MEMORY,
    };
    parser.* = Parser.init(std.heap.c_allocator);

    out_parser.* = parser;
    return .ULG_RESULT_OK;
}

export fn ulg_parser_parse(parser: *ulg_parser, bytes_ptr: [*c]const u8, bytes_len: usize) ulg_result {
    const bytes = bytes_ptr[0..bytes_len];
    parser.parse(bytes) catch |err| switch (err) {
        error.OutOfMemory => return .ULG_RESULT_OUT_OF_MEMORY,
        error.InvalidMagic => return .ULG_RESULT_INVALID_MAGIC,
        error.InvalidVersion => return .ULG_RESULT_INVALID_VERSION,
        error.InvalidFieldFormat => return .ULG_RESULT_INVALID_FIELD_FORMAT,
    };

    return .ULG_RESULT_OK;
}

export fn ulg_parser_finalize(parser: *ulg_parser, out_container: **const ulg_container) ulg_result {
    const container = std.heap.c_allocator.create(ulg_container) catch |err| switch (err) {
        error.OutOfMemory => return .ULG_RESULT_OUT_OF_MEMORY,
    };
    container.* = parser.finalize() catch |err| switch (err) {
        error.OutOfMemory => return .ULG_RESULT_OUT_OF_MEMORY,
    };

    out_container.* = container;
    return .ULG_RESULT_OK;
}

export fn ulg_container_get_type_handle(
    container: *ulg_container,
    name_ptr: [*c]const u8,
    name_len: usize,
    out_handle: *u32,
) ulg_result {
    const name = name_ptr[0..name_len];
    if (container.getHandle(name)) |handle| {
        out_handle.* = @intFromEnum(handle);
        return .ULG_RESULT_OK;
    } else {
        return .ULG_RESULT_UNKNOWN_TYPE;
    }
}

export fn ulg_container_count_subscriptions(container: *ulg_container) u32 {
    return @intCast(container.subscriptions.len);
}

export fn ulg_container_find_subscription(
    container: *ulg_container,
    ty: u32,
    out_index: *u32,
) ulg_result {
    out_index.* = container.findSubscription(@enumFromInt(ty)) catch |err| switch (err) {
        error.UnknownSubscription => return .ULG_RESULT_UNKNOWN_SUBSCRIPTION,
    };

    return .ULG_RESULT_OK;
}

export fn ulg_container_get_subscription(
    container: *ulg_container,
    index: u32,
) *const ulg_subscription {
    return &container.subscriptions[index];
}

export fn ulg_container_get_fieldset(
    container: *ulg_container,
    ty: u32,
) ulg_fieldset {
    return container.fieldset(@enumFromInt(ty)).?;
}
