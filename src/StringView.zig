const std = @import("std");

const StringView = @This();

start: u32,
len: u32,

pub const Context = struct {
    str_bytes: *std.ArrayListUnmanaged(u8),

    pub fn eql(_: Context, a: StringView, b: StringView) bool {
        return std.meta.eql(a, b);
    }

    pub fn hash(self: Context, x: StringView) u64 {
        const slice = self.str_bytes.items[x.start .. x.start + x.len];
        return std.hash_map.hashString(slice);
    }
};

pub const ArrayContext = struct {
    str_bytes: *std.ArrayListUnmanaged(u8),

    pub fn eql(_: ArrayContext, a: StringView, b: StringView, _: usize) bool {
        return std.meta.eql(a, b);
    }

    pub fn hash(self: ArrayContext, x: StringView) u32 {
        const slice = self.str_bytes.items[x.start .. x.start + x.len];
        return @truncate(std.hash_map.hashString(slice));
    }
};

pub const SliceAdapter = struct {
    str_bytes: *std.ArrayListUnmanaged(u8),

    pub fn eql(self: SliceAdapter, a_slice: []const u8, b: StringView) bool {
        const b_slice = self.str_bytes.items[b.start .. b.start + b.len];
        return std.mem.eql(u8, a_slice, b_slice);
    }

    pub fn hash(_: SliceAdapter, x: []const u8) u64 {
        return std.hash_map.hashString(x);
    }
};

pub const ArraySliceAdapter = struct {
    str_bytes: []const u8,

    pub fn eql(self: ArraySliceAdapter, a_slice: []const u8, b: StringView, _: usize) bool {
        const b_slice = self.str_bytes[b.start .. b.start + b.len];
        return std.mem.eql(u8, a_slice, b_slice);
    }

    pub fn hash(_: ArraySliceAdapter, x: []const u8) u32 {
        return @truncate(std.hash_map.hashString(x));
    }
};
