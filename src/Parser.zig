const std = @import("std");
const Allocator = std.mem.Allocator;
const Container = @import("Container.zig");
const StringView = @import("StringView.zig");
const testing = std.testing;

const Parser = @This();
const Type = Container.Type;
const FieldSet = Container.FieldSet;

const header_size = 7 + 1 + 8;
const flags_size = 2 + 1 + 8 + 8 + 24;
const endian: std.builtin.Endian = .little;

gpa: Allocator,
state: State,
payload_size: u16,
buffer: std.ArrayListUnmanaged(u8),

timestamp: u64,
str_bytes: std.ArrayListUnmanaged(u8),
info: std.HashMapUnmanaged(StringView, StringView, StringView.Context, std.hash_map.default_max_load_percentage),
fields: std.ArrayListUnmanaged(Field),
types: std.ArrayHashMapUnmanaged(StringView, FieldSet, StringView.ArrayContext, true),
subscriptions: std.ArrayListUnmanaged(Subscription),
logs: std.ArrayListUnmanaged(Log),
dropouts: std.ArrayListUnmanaged(Dropout),

const Subscription = struct {
    multi_id: u8,
    message_type: Type,
    message_size: usize,
    message_count: usize,
    data: std.ArrayListAlignedUnmanaged(u8, @alignOf(u64)),
};

pub const Field = struct {
    name: StringView,
    count: usize,
    ty: Type,
};

const Log = struct {
    timestamp: u64,
    level: Level,
    message: StringView,

    const Level = enum(u8) {
        emerg,
        alert,
        crit,
        err,
        warning,
        notice,
        info,
        debug,
        _,
    };
};

const Dropout = Container.Dropout;

const State = enum {
    header,
    flags,
    msg_size,
    msg_type,
    msg_payload,
};

pub fn init(gpa: Allocator) Parser {
    return .{
        .gpa = gpa,
        .state = .header,
        .payload_size = undefined,
        .buffer = .{},
        .timestamp = undefined,
        .str_bytes = .{},
        .info = .{},
        .fields = .{},
        .types = .{},
        .subscriptions = .{},
        .logs = .{},
        .dropouts = .{},
    };
}

pub fn parse(p: *Parser, bytes: []const u8) !void {
    var pos: usize = 0;
    while (pos < bytes.len) {
        switch (p.state) {
            .header => if (try p.take(bytes, &pos, header_size)) {
                const timestamp = try parseHeader(p.buffer.items);
                p.timestamp = timestamp;

                p.buffer.clearRetainingCapacity();
                p.state = .flags;
            },
            .flags => if (try p.take(bytes, &pos, flags_size)) {
                p.buffer.clearRetainingCapacity();
                p.state = .msg_size;
            },
            .msg_size => if (try p.take(bytes, &pos, @sizeOf(u16))) {
                p.payload_size = std.mem.readInt(u16, p.buffer.items[0..2], endian);
                p.buffer.clearRetainingCapacity();
                try p.buffer.ensureTotalCapacity(p.gpa, p.payload_size + 1);
                p.state = .msg_type;
            },
            .msg_type => {
                p.buffer.appendAssumeCapacity(bytes[pos]);
                pos += 1;
                p.state = .msg_payload;
            },
            .msg_payload => if (try p.take(bytes, &pos, p.payload_size + 1)) {
                const msg_type = p.buffer.items[0];
                const payload_bytes = p.buffer.items[1..p.buffer.items.len];
                try p.parseMessage(msg_type, payload_bytes);

                p.buffer.clearRetainingCapacity();
                p.state = .msg_size;
            },
        }
    }
    //
    // var it = p.info.iterator();
    // while (it.next()) |entry| {
    //     const key_view = entry.key_ptr.*;
    //     const value_view = entry.value_ptr.*;
    //     const key = p.str_bytes.items[key_view.start .. key_view.start + key_view.len];
    //     const value = p.str_bytes.items[value_view.start .. value_view.start + value_view.len];
    //     std.debug.print("{s} -> {s}\n", .{ key, value });
    // }
}

pub fn deinit(p: *Parser) void {
    p.buffer.deinit(p.gpa);
    p.str_bytes.deinit(p.gpa);
    p.info.deinit(p.gpa);
    p.fields.deinit(p.gpa);
    p.types.deinit(p.gpa);
    for (p.subscriptions.items) |*sub| sub.data.deinit(p.gpa);
    p.subscriptions.deinit(p.gpa);
}

pub fn finalize(p: *Parser) !Container {
    const str_bytes = try p.str_bytes.toOwnedSlice(p.gpa);

    const subscriptions = try p.gpa.alloc(Container.Subscription, p.subscriptions.items.len);
    defer p.subscriptions.deinit(p.gpa);
    for (p.subscriptions.items, 0..) |*sub, i| {
        const data = try sub.data.toOwnedSlice(p.gpa);
        subscriptions[i] = .{
            .multi_id = sub.multi_id,
            .message_type = sub.message_type,
            .message_size = sub.message_size,
            .message_count = sub.message_count,
            .data_ptr = data.ptr,
            .data_len = data.len,
        };
    }

    const fields = try p.gpa.alloc(Container.Field, p.fields.items.len);
    defer p.fields.deinit(p.gpa);
    for (p.fields.items, 0..) |*field, i| {
        const name = str_bytes[field.name.start .. field.name.start + field.name.len];
        fields[i] = .{
            .name_ptr = name.ptr,
            .name_len = name.len,
            .count = field.count,
            .type = field.ty,
        };
    }

    const logs = try p.gpa.alloc(Container.Log, p.logs.items.len);
    defer p.logs.deinit(p.gpa);
    for (p.logs.items, 0..) |*log, i| {
        const message = str_bytes[log.message.start .. log.message.start + log.message.len];
        logs[i] = .{
            .timestamp = log.timestamp,
            .level = @enumFromInt(@intFromEnum(log.level)),
            .message_ptr = message.ptr,
            .message_len = message.len,
        };
    }

    return .{
        .gpa = p.gpa,
        .timestamp = p.timestamp,
        .str_bytes = str_bytes,
        .subscriptions = subscriptions,
        .fields = fields,
        .types = p.types,
        .logs = logs,
        .dropouts = try p.dropouts.toOwnedSlice(p.gpa),
    };
}

fn take(p: *Parser, bytes: []const u8, pos: *usize, max_count: usize) !bool {
    // fills up the buffer with as many bytes as available until
    // max_count is reached
    const remaining = max_count - p.buffer.items.len;
    if (remaining == 0) return true;

    const count = @min(remaining, bytes.len - pos.*);
    try p.buffer.ensureUnusedCapacity(p.gpa, count);

    const taken = bytes[pos.* .. pos.* + count];
    p.buffer.appendSliceAssumeCapacity(taken);
    pos.* += count;

    std.debug.assert(p.buffer.items.len <= max_count);
    return p.buffer.items.len == max_count;
}

fn storeString(p: *Parser, bytes: []const u8) !StringView {
    const start: u32 = @intCast(p.str_bytes.items.len);
    try p.str_bytes.appendSlice(p.gpa, bytes);
    const end: u32 = @intCast(p.str_bytes.items.len);

    return .{ .start = start, .len = end - start };
}

fn parseHeader(bytes: []const u8) !u64 {
    std.debug.assert(bytes.len == header_size);
    const magic = [_]u8{ 0x55, 0x4c, 0x6f, 0x67, 0x01, 0x12, 0x35 };
    comptime var pos: usize = 0;

    if (!std.mem.eql(u8, &magic, bytes[pos .. pos + magic.len])) return error.InvalidMagic;
    pos += magic.len;
    if (bytes[pos] != 0x01) return error.InvalidVersion;
    pos += 1;

    const timestamp = std.mem.readInt(u64, bytes[pos .. pos + @sizeOf(u64)], endian);
    return timestamp;
}

fn parseMessage(p: *Parser, msg_type: u8, payload_bytes: []const u8) !void {
    switch (msg_type) {
        // Format Message
        'F' => {
            var split_colon = std.mem.splitScalar(u8, payload_bytes, ':');
            if (std.mem.eql(u8, split_colon.peek().?, "vehicle_status")) std.debug.print("{s}\n", .{payload_bytes});
            const message_name = try p.storeString(split_colon.next().?);
            const message_type = split_colon.rest();

            const fields_start: u32 = @intCast(p.fields.items.len);
            var split_semi = std.mem.splitScalar(u8, message_type, ';');
            while (split_semi.next()) |field_bytes| {
                if (field_bytes.len == 0) break;
                var split_space = std.mem.splitScalar(u8, field_bytes, ' ');

                const field_type = try parseFieldType(split_space.next().?);
                const base = field_type.base;
                const count = field_type.count;
                const field_name = try p.storeString(split_space.rest());

                const adapter: StringView.ArraySliceAdapter = .{ .str_bytes = p.str_bytes.items };
                const ty = if (builtin_types.get(base)) |builtin_type| ty: {
                    break :ty builtin_type;
                } else if (p.types.getIndexAdapted(base, adapter)) |type_index| ty: {
                    break :ty @as(Type, @enumFromInt(std.meta.fields(Type).len + type_index));
                } else ty: {
                    // forward-declare the type
                    const type_name = try p.storeString(base);
                    const type_index: u32 = @intCast(p.types.entries.len);
                    const context: StringView.ArrayContext = .{ .str_bytes = &p.str_bytes };
                    try p.types.putContext(p.gpa, type_name, undefined, context);

                    break :ty @as(Type, @enumFromInt(std.meta.fields(Type).len + type_index));
                };

                const field: Field = .{
                    .name = field_name,
                    .count = count,
                    .ty = ty,
                };
                try p.fields.append(p.gpa, field);
            }

            const fields_end: u32 = @intCast(p.fields.items.len);
            const aggregate: FieldSet = .{
                .start = fields_start,
                .end = fields_end,
            };

            const context: StringView.ArrayContext = .{ .str_bytes = &p.str_bytes };
            try p.types.putContext(p.gpa, message_name, aggregate, context);
        },
        // Information Message
        'I' => {
            // TODO: this only supports string fields currently
            const key_len = std.mem.readInt(u8, payload_bytes[0..1], endian);
            const key_bytes = payload_bytes[1 .. 1 + key_len];
            const value_bytes = payload_bytes[1 + key_len .. payload_bytes.len];

            const key = try p.storeString(key_bytes);
            const value = try p.storeString(value_bytes);

            const context: StringView.Context = .{ .str_bytes = &p.str_bytes };
            try p.info.putContext(p.gpa, key, value, context);
        },
        // Subscription Message
        'A' => {
            comptime var pos: usize = 0;
            const multi_id = std.mem.readInt(u8, payload_bytes[pos .. pos + @sizeOf(u8)], endian);
            pos += @sizeOf(u8);
            const msg_id = std.mem.readInt(u16, payload_bytes[pos .. pos + @sizeOf(u16)], endian);
            pos += @sizeOf(u16);
            const message_name = payload_bytes[pos..payload_bytes.len];

            const adapter: StringView.ArraySliceAdapter = .{ .str_bytes = p.str_bytes.items };
            const type_index = p.types.getIndexAdapted(message_name, adapter).?;
            const ty: Type = @enumFromInt(std.meta.fields(Type).len + type_index);

            std.debug.assert(msg_id == p.subscriptions.items.len);
            // std.debug.print("sub\n", .{});
            try p.subscriptions.append(p.gpa, .{
                .multi_id = multi_id,
                .message_type = ty,
                .message_size = p.getSize(ty),
                .message_count = 0,
                .data = .{},
            });
        },
        // Logged Data Message
        'D' => {
            comptime var pos: usize = 0;
            const msg_id = std.mem.readInt(u16, payload_bytes[pos .. pos + @sizeOf(u16)], endian);
            pos += @sizeOf(u16);
            const message_data = payload_bytes[pos..payload_bytes.len];

            // TODO: ending padding is not logged, should be inserted
            const sub_data = &p.subscriptions.items[msg_id].data;
            try sub_data.appendSlice(p.gpa, message_data);

            // add end of struct padding (omitted in log file for space savings)
            const message_size = p.subscriptions.items[msg_id].message_size;
            if (message_data.len < message_size) {
                _ = try sub_data.addManyAsSlice(p.gpa, message_size - message_data.len);
            }

            p.subscriptions.items[msg_id].message_count += 1;
        },
        // Logged String Message
        'L' => {
            comptime var pos: usize = 0;
            const log_level = std.mem.readInt(u8, payload_bytes[pos .. pos + @sizeOf(u8)], endian);
            pos += @sizeOf(u8);
            const timestamp = std.mem.readInt(u64, payload_bytes[pos .. pos + @sizeOf(u64)], endian);
            pos += @sizeOf(u64);
            const message_bytes = payload_bytes[pos..payload_bytes.len];

            const message = try p.storeString(message_bytes);

            try p.logs.append(p.gpa, .{
                .timestamp = timestamp,
                .level = @enumFromInt(log_level),
                .message = message,
            });
        },
        else => {
            // parser must ignore unknown message formats
            // std.debug.print("ignoring: {c}\n", .{msg_type});
        },
    }
}

fn parseFieldType(bytes: []const u8) !struct { base: []const u8, count: usize } {
    var base = bytes;
    var count: usize = 1;
    if (std.mem.indexOfScalar(u8, bytes, '[')) |l_bracket| {
        const r_bracket = std.mem.indexOfScalar(u8, bytes, ']').?;
        base = bytes[0..l_bracket];
        count = std.fmt.parseInt(usize, bytes[l_bracket + 1 .. r_bracket], 10) catch return error.InvalidFieldFormat;
    }

    return .{ .base = base, .count = count };
}

fn getSize(p: *Parser, ty: Type) usize {
    return switch (ty) {
        .int8, .uint8 => 1,
        .int16, .uint16 => 2,
        .int32, .uint32 => 4,
        .int64, .uint64 => 8,
        .float => 4,
        .double => 8,
        .bool, .char => 1,
        else => {
            const index = @intFromEnum(ty) - std.meta.fields(Type).len;
            const aggregate = p.types.values()[index];

            const fields = p.fields.items[aggregate.start..aggregate.end];
            var total: usize = 0;
            for (fields) |field| {
                total += p.getSize(field.ty) * field.count;
            }

            return total;
        },
    };
}

pub const builtin_types = std.StaticStringMap(Type).initComptime(.{
    .{ "int8_t", .int8 },
    .{ "uint8_t", .uint8 },
    .{ "int16_t", .int16 },
    .{ "uint16_t", .uint16 },
    .{ "int32_t", .int32 },
    .{ "uint32_t", .uint32 },
    .{ "int64_t", .int64 },
    .{ "uint64_t", .uint64 },
    .{ "float", .float },
    .{ "double", .double },
    .{ "bool", .bool },
    .{ "char", .char },
});

// test "batched header parsing" {
//     var parser = Parser.init(std.testing.allocator);
//     defer parser.deinit();
//
//     const valid = [_]u8{ 0x55, 0x4c, 0x6f, 0x67, 0x01, 0x12, 0x35, 0x01, 0x78, 0x056, 0x34, 0x12, 0x00, 0x00, 0x00, 0x00 };
//
// }

// test "basic testing" {
//     var parser = Parser.init(std.testing.allocator);
//     defer parser.deinit();
//
//     const file = @embedFile("22_36_26.ulg");
//     try parser.parse(file);
// }
