#include <stdio.h>
#include <stdlib.h>

#include "libulog.h"

void fail(enum ulg_result result) {
  switch (result) {
  case ULG_RESULT_OK:
    printf("parser: ok");
    break;
  case ULG_RESULT_OUT_OF_MEMORY:
    printf("parser: out of memory\n");
    break;
  case ULG_RESULT_INVALID_MAGIC:
    printf("parser: invalid magic\n");
    break;
  case ULG_RESULT_INVALID_VERSION:
    printf("parser: invalid version\n");
    break;
  case ULG_RESULT_UNKNOWN_TYPE:
    printf("parser: unknown type\n");
    break;
  case ULG_RESULT_INVALID_FIELD_FORMAT:
    printf("parser: field format\n");
    break;
  case ULG_RESULT_UNKNOWN_SUBSCRIPTION:
    printf("parser: unknown subscription\n");
    break;
  }

  exit(1);
}

int main(int argc, char **argv) {
  // not clean example - no input validation, doesn't destroy objects
  // on failure

  FILE *file = fopen(argv[1], "r");

  char buffer[4096];
  size_t count;
  enum ulg_result result;

  struct ulg_parser *parser;
  result = ulg_parser_create(&parser);
  if (result != 0)
    fail(result);

  while ((count = fread(buffer, 1, sizeof buffer, file)) != 0) {
    result = ulg_parser_parse(parser, buffer, count);
    if (result != 0)
      fail(result);
  }

  struct ulg_container *container;
  result = ulg_parser_finalize(parser, &container);

  uint32_t vehicle_status_handle;
  result = ulg_container_get_type_handle(container, "vehicle_status", 14,
                                         &vehicle_status_handle);
  if (result != 0)
    fail(result);

  uint32_t vehicle_status_subi;
  result = ulg_container_find_subscription(container, vehicle_status_handle,
                                           &vehicle_status_subi);
  if (result != 0)
    fail(result);

  struct ulg_subscription const *vehicle_status_sub =
      ulg_container_get_subscription(container, vehicle_status_subi);
  if (result != 0)
    fail(result);

  printf("vehicle_status size: %ld\n",
         ulg_container_get_type_size(container, vehicle_status_handle));
  printf("vehicle_status messages: %ld\n", vehicle_status_sub->message_count);

  ulg_container_destroy(container);

  fclose(file);

  return 0;
}
