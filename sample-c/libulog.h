#ifndef _LIB_ULOG_H
#define _LIB_ULOG_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

struct ulg_parser;    // opaque
struct ulg_container; // opaque

struct ulg_subscription {
  uint8_t multi_id;
  uint32_t message_type;
  size_t message_size;
  size_t message_count;
  void *data_ptr; // aligned to uint64_t
  size_t data_len;
};

struct ulg_field {
  uint8_t const *name_ptr;
  size_t name_len;
  uint32_t type;
  size_t count;
};

struct ulg_field_iterator {
  bool recursive;
};

// ulg_type
struct ulg_dropout {};

struct ulg_log {};

struct ulg_fieldset {
  uint32_t start;
  uint32_t end;
};

enum ulg_result {
  ULG_RESULT_OK = 0,
  ULG_RESULT_OUT_OF_MEMORY = -1,
  ULG_RESULT_INVALID_MAGIC = -2,
  ULG_RESULT_INVALID_VERSION = -3,
  ULG_RESULT_UNKNOWN_TYPE = -4,
  ULG_RESULT_INVALID_FIELD_FORMAT = -5,
  ULG_RESULT_UNKNOWN_SUBSCRIPTION = -6,
};

enum ulg_result ulg_parser_create(struct ulg_parser **out_parser);
enum ulg_result ulg_parser_parse(struct ulg_parser *parser,
                                 void const *bytes_ptr, size_t bytes_len);
enum ulg_result ulg_parser_finalize(struct ulg_parser *parser,
                                    struct ulg_container **out_container);

enum ulg_result ulg_container_get_type_handle(struct ulg_container *container,
                                              char const *name_ptr,
                                              size_t name_len,
                                              uint32_t *out_handle);
size_t ulg_container_get_type_size(struct ulg_container *container,
                                   uint32_t type);

uint32_t ulg_container_count_subscriptions(struct ulg_container *container);
enum ulg_result ulg_container_find_subscription(struct ulg_container *container,
                                                uint32_t type,
                                                uint32_t *out_index);
struct ulg_subscription const *
ulg_container_get_subscription(struct ulg_container *container, uint32_t index);

/* struct ulg_fieldset */

void ulg_container_destroy(struct ulg_container *container);

#endif /* _LIB_ULOG_H */
